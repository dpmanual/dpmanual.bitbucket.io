/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

/////////////////////////// GLOBALS ////////////////////////////
float m; // motion value
int mode = 0;
int gridStep = 40;
boolean isBlur;
/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(500, 500);
  background(0);
  noStroke();
  PFont f = createFont("IBMPlexMono-Light", 12);
  textFont(f);
  textSize(12);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  if(isBlur){
    fill(0, 33);
    rect(0,0,width, height);
  }else {
    background(0);
  }
  fill(255);
  float f = map(mouseX, 0, width, 0.025, 0.075);
  float amp = map(mouseY, 0, height, 25, 75);
  for (int y=25; y<height-25; y+=gridStep) {
  for (int x=25; x<width-25; x+=gridStep) {
    switch(mode){
      case 0:
      m = sin( (x + y) + frameCount * f) * amp;
      ellipse(x, y, m, m);
      break;
      
      case 1:
      m = sin( (x * y) + frameCount * f) * amp;
      ellipse(x, y, m, m);
      break;
      
      case 2:
      m = sin( (x * f) + frameCount * f) * amp;
      ellipse(x, y, m, m);
      break;
      
      case 3:
      m = sin( (x * f) + frameCount * f) * amp;
      ellipse(x + m, y, m, m);
      break;
      
      case 4:
      m = sin( (x * f) + frameCount * f) * amp;
      ellipse(x, y + m, m, m);
      break;
      
      case 5:
      m = sin( (y * f) + frameCount * f) * amp;
      ellipse(x + m, y, m, m);
      break;
      
      case 6:
      m = sin( (y * f) + frameCount * f) * amp;
      float m2 = sin( (x * f) + frameCount * f) * amp;
      ellipse(x + m + m2, y, m, m);
      break;
          
      }
    }
  }
  
  text("Motion Mode: "+mode, 15, height-20);
}


void keyPressed() {
  if (key == 's') {
    saveFrame("export_DPMOsc_002_###.png");
  }
   if (key == 'b') {
    isBlur = !isBlur;
  }
  if(key == '+'){
    if(mode<6){
      mode++;
    }
  }
  if(key == '-'){
   if(mode>0){
    mode--; 
   }
  }
      if (key == 'l') {
        gridStep += 5;
    }
    if (key == 'm') {
        if (gridStep > 5)
            gridStep -= 5;
    }
}