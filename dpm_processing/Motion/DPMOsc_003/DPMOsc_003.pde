/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

/////////////////////////// GLOBALS ////////////////////////////
int gridStep = 12;
int mode = 0;

/////////////////////////// SETUP ////////////////////////////
void setup() {
  size(500, 500);
  background(0);
  noStroke();
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  fill(0, 33);
  rect(0, 0, width, height);
  fill(255);
  float v = map(mouseX, 0, width, 0.015, 0.1);
  for (int x=0; x<width; x+=gridStep) {
    float h = sin( x + frameCount * 0.05) * 150;   
    float y = sin( (x * v)  + frameCount * 0.025) * 50; 
    switch(mode) {
    case 0:
      rect(x, height/2 + y, gridStep, h);
      break;
    case 1:
      rect(width/2+y, x, h, gridStep);
      break;
    }
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////
void keyPressed() {
  if (key == 's') {
    saveFrame("export_DPMOsc_003_###.png");
  }
  if (key == '+') {
    mode++;
  }
  if (key == '-') {
    if (mode>0) {
      mode--;
    }
  }
  if (key == 'l') {
    gridStep+=2;
  }
  if (key == 'm') {
    if (gridStep>2)
      gridStep-=2;
  }
}