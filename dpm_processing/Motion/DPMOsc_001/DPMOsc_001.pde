/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

/////////////////////////// GLOBALS ////////////////////////////
float m; // motion value
int mode = 0;
int gridStep = 40;
/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(500, 500);
  background(0);
  noStroke();
  PFont f = createFont("IBMPlexMono-Light", 12);
  textFont(f);
  textSize(12);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);
  fill(255);
  float f = map(mouseX, 0, width, 0.025, 0.075);
  float amp = map(mouseY, 0, height, 25, 175);
  for (int x=50; x<width-50; x+=gridStep) {
    switch(mode) {
    case 0:
      m = sin( x  + frameCount * 0.020) * 120; 
      float d = sin( x  + frameCount * 0.10) * 90;
      ellipse(x, height/2 + m, d, d);
      break;

    case 1:
      m = sin( x  + frameCount * f) * amp; 
      ellipse(x, height/2 + m, m/4, m/4);
      break;
    }
  }
  text("MODE: "+mode, 15, height-20);
}


void keyPressed() {
  if (key == 's') {
    saveFrame("export_DPMOsc_002_###.png");
  }

  if (key == '+') {
    mode++;
  }
  if (key == '-') {
    if (mode>0) {
      mode--;
    }
  }
  if (key == 'l') {
    gridStep+=5;
  }
  if (key == 'm') {
    if (gridStep>5)
      gridStep-=5;
  }
}