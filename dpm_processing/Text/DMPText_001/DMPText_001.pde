/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */


/////////////////////////// GLOBALS ////////////////////////////
PFont f;
String word = "BAUHAUS";
/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(500, 500);
  background(0);
  noStroke();
  f = createFont("Helvetica-Bold", 100);
  textFont(f, 80);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);
  int maxIteration = (int)map(mouseX, 0, width, 75, height);
  int interval = (int)map(mouseY, 0, height, 1, 100);

  for (int i=75; i<maxIteration; i+=interval) {
    int gradient = (int)map(i, 75, height, 0, 255);
    fill(gradient);
    text(word, 50, i);
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////

void keyPressed() {
  if (key == 's') {
    saveFrame("export_###.png");
  }
}