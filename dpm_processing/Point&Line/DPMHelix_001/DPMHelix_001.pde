/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

float sw = 1.0;
boolean isInteractive = false;

/////////////////////////// SETUP ////////////////////////////
void setup() {
  size(500, 500);
  noFill();
  stroke(255);
  strokeCap(SQUARE);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);
  strokeWeight(sw);
  int num = (int)map(mouseY, 0, height, 1, 150);
  drawForm(width/2, height/2, num);
}

/////////////////////////// FUNCTIONS ////////////////////////////
void drawForm(float _x, float _y, int _num) {
  pushMatrix();
  translate(_x, _y);
  float angle = 0;
  for (int i = 0; i< _num; i++) {
    float orbitX = 50;
    float orbitY = 10;
    float y = map(i, 0.0, _num, -((height/2.0)-orbitY), (height/2.0) - orbitY);
    float delay = map(i, 0.0, _num, -1.0, 1.0);
    if (isInteractive) {
      angle = map(mouseX, 0.0, width, -1.0, 1.0);
    } else {
      angle = frameCount*0.015;
    }
    float x1 = sin(delay + angle) * orbitX;
    float y1 = y + (cos(delay + angle)) * orbitY;
    float x2 = sin(delay + angle) * (-orbitX);
    float y2 = y + (cos(delay + angle)) * (-orbitY);

    line(x1, y1, x2, y2);
  }

  popMatrix();
}


void keyPressed() {
  if (key == 's') {
    saveFrame("savedImage_###.png");
  }
  if (key == '+') sw++;
  if (key == '-') {
    if (sw>0)
      sw--;
  }
  if (key =='i') {
    isInteractive = !isInteractive;
  }
}