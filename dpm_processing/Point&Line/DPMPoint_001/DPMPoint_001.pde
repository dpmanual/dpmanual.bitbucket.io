/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */


/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(500, 500);
  background(0);
  stroke(255);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);
  float pntSize = map(sin(frameCount * 0.012), -1, 1, 0, 600);
  strokeWeight(pntSize);
  point(width/2, height/2);
}

/////////////////////////// FUNCTIONS ////////////////////////////


void keyPressed() {
  if (key == 's') saveFrame("savedImage_###.png");
}