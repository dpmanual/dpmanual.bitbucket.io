/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

/////////////////////////// GLOBALS ////////////////////////////

int gridStep = 55; // gap between each
int lineLen = 50; // length of lines
int lineDensity = 10; // number of repetitions
float strokeWidth = 1.5; // >>>>>>>>>>>>>>> added key interactions
int seed;
/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(500, 500);
  background(0);
  smooth();
  stroke(255);
  strokeCap(SQUARE);
  seed = (int)random(1000);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);
  randomSeed(seed);
  strokeWeight(strokeWidth);

  for (int y=0; y<=height; y+=gridStep) {
    for (int x=0; x<=width; x+=gridStep) {
      int randVal = (int)random(12);   
      float cutOffValX = map(mouseX, 0, width, 0, 12);
      float cutOffValY = map(mouseY, 0, height, 0, 12);
      if (randVal <= cutOffValX) {
        drawLines(x, y, lineLen, true);
      }
      if (randVal > cutOffValY) {
        drawLines(x, y, lineLen, false);
      }
    }
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////
void drawLines(int _x, int _y, int _len, boolean _isVertical) {
  int inter = gridStep/lineDensity;
  for (int i=0; i<gridStep; i+=inter) {
    if (_isVertical) {
      line(_x+i, _y-_len, _x+i, _y+_len);
    } else {
      line(_x-_len, _y+i, _x+_len, _y+i);
    }
  }
}

void keyPressed() {
  if (key == 's') {
    saveFrame("savedImage_###.png");
  }
  if (key == 'r') {
    seed = (int)random(1000);
  }
  if (key == '+') {
    gridStep+=5;
  }
  if (key == '-') {
    if (gridStep>5) {
      gridStep-=5;
    }
  }

    if (key == 'w') {
      lineLen +=5;
    }
    if (key == 'x') {
      if (lineLen>0) {
        lineLen -=5;
      }
    }

    if (key == 'l') {
      lineDensity+=1;
    }
    
    if (key == 'm') {
      if (lineDensity>0) {
        lineDensity -=1;
      }
    }
    
      if (key == 'a') {
      strokeWidth+=0.2;
    }
    
    if (key == 'z') {
      if (strokeWidth>0.2) {
        strokeWidth -=0.2;
      }
    }
}
