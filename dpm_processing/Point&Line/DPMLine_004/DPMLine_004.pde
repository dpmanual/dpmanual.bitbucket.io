/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */


float lenMax = 260;
float thickness = 10;
float angleStep;
int numRepeats = 35;
int randSeed;

/////////////////////////// SETUP ////////////////////////////
void setup() {
  size(500, 500);
  strokeCap(SQUARE);
  stroke(255);
  randSeed = (int)random(1000);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  randomSeed(randSeed);
  background(0);
  translate(width/2, height/2);
  numRepeats = (int)map(mouseX, 0, width, 5, 150);
  angleStep = 360/numRepeats;
  thickness = map(mouseY, 0, height, .25, 25);
  strokeWeight( thickness ); 
  for (int i=0; i<360; i+=angleStep) {
    float x = cos(radians(i)) * 50;
    float y = sin(radians(i)) * 50;
    float len = random(3, lenMax);
    float x2 = cos(radians(i)) * len;
    float y2 = sin(radians(i)) * len;
    pushMatrix();   
    translate(x, y);
    line(0, 0, x2, y2);
    popMatrix();
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////
void keyPressed() {
  if (key == 'r') {
    randSeed = (int)random(1000);
  }
  if (key == 's') {
    saveFrame("export_###.png");
  }
}