/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

int sw = 1;

/////////////////////////// SETUP ////////////////////////////
void setup() {
  size(500, 500);
  noFill();
  stroke(255);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);

  int num = (int)map(mouseX, 0, width, 0, 150);
  float size = map(mouseY, 0, height, 0, 250);          
  strokeWeight(sw);
  translate(width/2, height/2);
  for (int i = 0; i< num; i++) {
   float degree = map(i, 0.0, num, 0.0, 360.0);
   pushMatrix();
   translate(0, 0);
    rotate(radians(degree));
    line(0, size, width/1.0, size);
    popMatrix();
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////
void keyPressed() {
  if (key == 's') {
    saveFrame("savedImage_###.png");
  }
  if (key == '+') sw++;
  if (key == '-') {
    if (sw>0)
      sw--;
  }
}