/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

/////////////////////////// SETUP ////////////////////////////
float sw = 1.0;

void setup() {
  size(500, 500);
  noFill();
  stroke(255);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);
  strokeWeight(sw);
  int num = (int)map(mouseY, 0, height, 75, 1);
  drawForm(width/2, height/2, num);
}

/////////////////////////// FUNCTIONS ////////////////////////////
void drawForm(float _x, float _y, int _num) {
  pushMatrix();
  translate(_x, _y);
  int d = mouseX;
  line(0,-height,0,height);
  for (int i = 0; i< width; i+=_num) {
    arc(0, 0,  d-i, height, 0, TWO_PI);
  }

  popMatrix();
}


void keyPressed() {
  if (key == 's') {
    saveFrame("savedImage_###.png");
  }
  if(key == '+') sw++;
  if(key == '-') {
    if(sw>0)
    sw--;
  }
}