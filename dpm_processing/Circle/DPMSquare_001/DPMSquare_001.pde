/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */


int num = 7;
boolean isAlternate = false;
/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(500, 500);
  background(0);
  noFill();
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);
    float sw = map(mouseY, 0, height, 1, 50);
    strokeWeight(sw);
    stroke(255);
    float angle = map(mouseX, 0, width, 0, 0.35);
    drawForm(width/2, height/2, angle, num);
}

/////////////////////////// FUNCTIONS ////////////////////////////
void drawForm(float _x, float _y, float _a, int _num) {
  pushMatrix();
  translate(_x, _y);
  rectMode(CENTER);
  int shpSize = _num*15;
  float a = 0;
  for (int i = 0; i< width; i++) {
    if(isAlternate){
      if(i%2==0){
        rotate(radians(a*i));
      }else{
        rotate(radians(-a*i));
      }
    }else {
      rotate(radians(a*i));
    }
    rect(0, 0, shpSize*i, shpSize*i);
    a+=_a;
  }
  popMatrix();
}


void keyPressed() {
  if (key == 's') saveFrame("savedImage_###.png");
  if(key == '+') {num++;}
  if(key == '-') {num--;}
  if(key == 'a'){isAlternate =!isAlternate;}
}