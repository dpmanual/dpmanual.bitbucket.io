/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(500, 500);
  noFill();
  stroke(255);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
    background(0);
    float sw = map(mouseY, 0, height, 0.05, 50);
    strokeWeight(sw);
    int num = (int)map(mouseX, 0, width, 20, 2);
    drawForm(width/2, height/2, num);
}

/////////////////////////// FUNCTIONS ////////////////////////////
void drawForm(float _x, float _y, int _num) {
  pushMatrix();
  translate(_x, _y);
  int dia = _num*15;
  for (int i = 0; i< width; i++) {
    //int dia = _num*i;
    ellipse(0, 0, dia*i, dia*i);
  }
  popMatrix();
}


void keyPressed() {
  if (key == 's') saveFrame("savedImage_###.png");
}