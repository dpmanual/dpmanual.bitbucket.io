/**
 *
 * Various tiles based on Edward Zajec's early computer art
 * REF : http://www.edwardzajec.com/tvc4/ser/index.html
 * REF : https://monoskop.org/Edward_Zajec
 */


void module_01(float x, float y, int taille, boolean isOutline) {
  pushMatrix();
  if (isOutline) {
    stroke(theCol);
  } else {
    noStroke();
  }
  noFill();
  translate(x, y);
  rect(0, 0, taille, taille);
  popMatrix();
}

void module_02(float x, float y, int taille, boolean isOutline) {
  pushMatrix();
  translate(-taille/2, -taille/2);
  stroke(theCol);
  if (isOutline) {
    noFill();
    rect(x+taille/2, y+taille/2, taille, taille);
  } else {
  }

  translate(x, y);
  line(0, taille/2, taille/2, 0);
  line(0, taille, taille, 0);
  int inter = taille/10;
  int xStart = taille;
  for (int j=0; j<taille; j+=inter) { 
    line(xStart, j, taille, j); 
    xStart -= inter;
  }
  popMatrix();
}

void module_03(float x, float y, int taille, boolean isOutline) {
  pushMatrix();
  translate(-taille/2, -taille/2);
  stroke(theCol);
  if (isOutline) {
    noFill();
    rect(x+taille/2, y+taille/2, taille, taille);
  } else {
  }

  fill(theCol);
  translate(x, y);
  strokeWeight(sw+0.5);
  triangle(0, taille, taille, taille, taille, 0); 
  int inter = taille/10;
  int xStart = taille;
    strokeWeight(sw);
  for (int j=0; j<=taille; j+=inter) { 
    line(0, j, xStart, j); 
    xStart -= inter;
  }
  popMatrix();
}

void module_04(float x, float y, int taille, boolean isOutline) {
  pushMatrix();
  translate(-taille/2, -taille/2);
  stroke(theCol);
  if (isOutline) {
    noFill();
    rect(x+taille/2, y+taille/2, taille, taille);
  } else {
  }

  fill(theCol);
  translate(x, y);
  triangle(0, 0, taille, 0, 0, taille);
  line(0, taille, taille, 0);
  line(taille/2, taille, taille, taille/2);
  popMatrix();
}

void module_05(float x, float y, int taille, boolean isOutline) {
  pushMatrix();
  translate(-taille/2, -taille/2);
  stroke(theCol);
  if (isOutline) {
    noFill();
    rect(x+taille/2, y+taille/2, taille, taille);
  } else {
  }

  noFill();
  translate(x, y);
  int inter = taille/10;
  for (int j=0; j<=taille; j+=inter) {
    line(0, j, taille, j);
  }

  popMatrix();
}

void module_06(float x, float y, int taille, boolean isStroke) {
  pushMatrix();
  if (isStroke) {
    stroke(theCol);
  } else {
    noStroke();
  }
  fill(theCol);
  translate(x, y);
  rect(0, 0, taille, taille);
  popMatrix();
}
void module_07(float x, float y, int taille, boolean isOutline) {
  pushMatrix();
  translate(-taille/2, -taille/2);
  stroke(theCol);
  if (isOutline) {
    noFill();
    rect(x+taille/2, y+taille/2, taille, taille);
  } else {
  }

  noFill();
  translate(x, y);
  line(0, taille/2, taille/2, 0);
  line(0, taille, taille, 0);
  line(taille/2, taille, taille, taille/2);
  popMatrix();
}

void module_08(float x, float y, int taille, boolean isOutline) {
  pushMatrix();
  translate(-taille/2, -taille/2);
  stroke(theCol);
  if (isOutline) {
    noFill();    
    rect(x+taille/2, y+taille/2, taille, taille);
  } else {
  }
  fill(theCol);
  strokeWeight(sw+0.5);
  translate(x, y); 
  triangle(0, 0, taille, 0, 0, taille);
  int inter = taille/10;
  int xStart = taille;
  //stroke(255);
  strokeWeight(sw);
  for (int j=0; j<=taille; j+=inter) { 
    line(xStart, j, taille, j); 
    xStart -= inter;
  }
  popMatrix();
}

/**
 * A function that draws modules
 * based on a number input from 0 > n (modules)
 */
void chooseModule(float x, float y, int t, boolean isOn, int rndNum) {
  
  if (rndNum==0) {
    module_01(x, y, t, isOn);
  }
  if (rndNum==1) {
    module_02(x, y, t, isOn);
  }
  if (rndNum==2) {
    module_03(x, y, t, isOn);
  }
  if (rndNum==3) {
    module_04(x, y, t, isOn);
  }
  if (rndNum==4) {
    module_05(x, y, t, isOn);
  }
  if (rndNum==5) {
    module_06(x, y, t, isOn);
  }
  if (rndNum==6) {
    module_07(x, y, t, isOn);
  }
  if (rndNum==7) {
    module_08(x, y, t, isOn);
  }
}