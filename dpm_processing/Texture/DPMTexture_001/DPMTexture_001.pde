/* 
 --------------------------
 ** PLEASE READ INFO TAB **
 --------------------------
 */

/////////////////////////// GLOBALS ////////////////////////////
int interval = 10;
/////////////////////////// SETUP ////////////////////////////

void setup() {
  size(500, 500);
  background(0);
  smooth();
  noStroke();
  fill(255);
}

/////////////////////////// DRAW ////////////////////////////
void draw() {
  background(0);
  translate(width/2, height/2);

  float dia2 = map(mouseY, 0, height, 1, 10);  
  drawGrid(dia2);
  rotate(mouseX*0.05);
  drawGrid(dia2);
}

/////////////////////////// FUNCTIONS ////////////////////////////

void drawGrid(float _d) {
  for (int y=-height/2*2; y<=height-50; y+=interval) {
    for (int x=-width/2*2; x<=width-50; x+=interval) {
      ellipse(x, y, _d, _d);
    }
  }
}

void keyPressed() {
  if (key =='s') {
    saveFrame("capture_###.png");
  }
   if (key =='+') {
    interval++;
  }
   if (key =='-') {
     interval--;
  }
}