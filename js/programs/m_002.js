/**
 * HARMONIC MOTION
 * Amandine
 * Sketch : oscillation_OffSet 
 */

let m; // motion value
let mode = 0;
let gridStep = 40;
let myFont;
let isBlur;

function preload() {
    myFont = loadFont('../font/IBMPlexMono-Bold.ttf');
}

function setup() {
    let cnv = createCanvas(500, 500);
    cnv.parent('theCanvas');
    noStroke();
    textFont(myFont);
    textSize(12);
}

function draw() {
    if (isBlur) {
        fill(0, 33);
        rect(0, 0, width, height);
    } else {
        background(0);
    }
    fill(255);
    let f = map(mouseX, 0, width, 0.025, 0.065);
    let amp = map(mouseY, 0, height, 10, 50);
    for (let y = 25; y < height - 25; y += gridStep) {
        for (let x = 25; x < width - 25; x += gridStep) {
            switch (mode) {
                case 0:
                    m = sin((x + y) + frameCount * f) * amp;
                    ellipse(x, y, m, m);
                    break;

                case 1:
                    m = sin((x * y) + frameCount * f) * amp;
                    ellipse(x, y, m, m);
                    break;

                case 2:
                    m = sin((x * f) + frameCount * f) * amp;
                    ellipse(x, y, m, m);
                    break;

                case 3:
                    m = sin((x * f) + frameCount * f) * amp;
                    ellipse(x + m, y, m, m);
                    break;

                case 4:
                    m = sin((x * f) + frameCount * f) * amp;
                    ellipse(x, y + m, m, m);
                    break;

                case 5:
                    m = sin((y * f) + frameCount * f) * amp;
                    ellipse(x + m, y, m, m);
                    break;

                case 6:
                    m = sin((y * f) + frameCount * f) * amp;
                    let m2 = sin((x * f) + frameCount * f) * amp;
                    ellipse(x + m + m2, y, m, m);
                    break;

            }
        }
    }

    text("Motion Mode: " + mode, 15, height - 20);
}

function keyPressed() {
    if (key == 'b') {
        isBlur = !isBlur;
    }
    if (key == '+') {
        if (mode < 6) {
            mode++;
        }
    }
    if (key == '-') {
        if (mode > 0) {
            mode--;
        }
    }
    if (key == 'l') {
        gridStep += 5;
    }
    if (key == 'm') {
        if (gridStep > 5)
            gridStep -= 5;
    }
}