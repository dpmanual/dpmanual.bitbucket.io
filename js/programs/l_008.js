/**
 * Point & Line: Curves
 * 
 * Sketch : DPM_Curves_001_p5js
 */



function setup() {
let cnv = createCanvas(500, 500);
  cnv.parent('theCanvas');
  noFill();
  stroke(255);
}

function draw() {
    background(0);   
    let num = map(mouseX, 0, width, 1, 75);
    let thickness = map(mouseY, 0, height, 1, 5);          
    strokeWeight(thickness);
    for (let i = 0; i< num; i++) {
        let linePositionX = map(i, 0.0, num, 10.0, width - 10.0);
      bezier(linePositionX, 0, mouseX, mouseY, mouseX, mouseY, linePositionX, height);
    }
}