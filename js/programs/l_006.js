/**
 * Point & Line: Shutter
 * 
 * Sketch : DPMShutter_001_p5js
 */


let sw = 1;

function setup() {
let cnv = createCanvas(500, 500);
  cnv.parent('theCanvas');
  noFill();
  stroke(255);
}

function draw() {
    background(0);   
  let num = map(mouseX, 0, width*2, 0, 500);
  let size = map(mouseY, 0, height, 0, 250);
  strokeWeight(sw);
  translate(width/2, height/2);
  for (let i = 0; i< num; i++) {
   let degree = map(i, 0.0, num, 0.0, 360.0);
   push();
   translate(0, 0);
    rotate(radians(degree));
    line(0, size, width/1.0, size);
    pop();
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////

function keyPressed() {
    if (key == '+') sw++;
    if (key == '-') {
      if (sw>0)
        sw--;
    }

}