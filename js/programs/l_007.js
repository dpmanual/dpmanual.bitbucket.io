/**
 * Point & Line: Helix
 * 
 * Sketch : DPMHelix_001_p5js
 */


let sw = 1;
let isInteractive = false;

function setup() {
let cnv = createCanvas(500, 500);
  cnv.parent('theCanvas');
  noFill();
  stroke(255);
  strokeCap(SQUARE);
}

function draw() {
    background(0);   
    strokeWeight(sw);
    let num = map(mouseY, 0, height, 1, 150);
    drawForm(width/2, height/2, num);
}


/////////////////////////// FUNCTIONS ////////////////////////////
function drawForm( _x,  _y,  _num) {
    push();
    translate(_x, _y);
    let angle = 0;
    for (let i = 0; i< _num; i++) {
      let orbitX = 50;
      let orbitY = 10;
      let y = map(i, 0.0, _num, -((height/2.0)-orbitY), (height/2.0) - orbitY);
      let delay = map(i, 0.0, _num, -1.0, 1.0);
      if (isInteractive) {
        angle = map(mouseX, 0.0, width, -1.0, 1.0);
      } else {
        angle = frameCount*0.015;
      }
      let x1 = sin(delay + angle) * orbitX;
      let y1 = y + (cos(delay + angle)) * orbitY;
      let x2 = sin(delay + angle) * (-orbitX);
      let y2 = y + (cos(delay + angle)) * (-orbitY);
  
      line(x1, y1, x2, y2);
    }
  
    pop();
  }
  
  
  function keyPressed() {
    if (key == 's') {
      saveFrame("savedImage_###.png");
    }
    if (key == '+') sw++;
    if (key == '-') {
      if (sw>0)
        sw--;
    }
    if (key =='i') {
      isInteractive = !isInteractive;
    }
  }