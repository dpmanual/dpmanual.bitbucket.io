/**
* Lines repeated on a circle.
*/

let lenMax = 180;
let thickness = 10;
let angleStep;
let numRepeats = 35;
let randSeed;

/////////////////////////// SETUP ////////////////////////////
function setup() {
  let cnv = createCanvas(500, 500);
  cnv.parent('theCanvas');
  strokeCap(SQUARE);
  stroke(255);
  randSeed = random(1000);
}

/////////////////////////// DRAW ////////////////////////////
function draw() {
  randomSeed(randSeed);
  background(0);
  translate(width/2, height/2);
  if(mouseX>0)
  numRepeats = map(mouseX, 0, width, 5, 150);
  angleStep = 360/numRepeats;
  thickness = map(mouseY, 0, height, .25, 25);
  strokeWeight( thickness ); 
  for (let i=0; i<360; i+=angleStep) {
    let x = cos(radians(i)) * 50;
    let y = sin(radians(i)) * 50;
    let len = random(3, lenMax);
    let x2 = cos(radians(i)) * len;
    let y2 = sin(radians(i)) * len;
    push();   
    translate(x, y);
    line(0, 0, x2, y2);
    pop();
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////
function keyPressed() {
  if (key == 'r') {
    randSeed = random(1000);
  }
  if (key == 's') {
    //saveFrame("export_###.png");
  }
}
