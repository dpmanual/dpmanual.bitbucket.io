/**
 * Circle: Arcs
 * 
 * Sketch : DPMArc_001_p5js
 */


let sw = 5;

function setup() {
let cnv = createCanvas(500, 500);
  cnv.parent('theCanvas');
  noFill();
  stroke(255);
}

function draw() {
    background(0);
    strokeWeight(sw);
    let num = map(mouseY, 0, height*2, 75, 1);
    drawForm(width/2, height/2, num);
}

/////////////////////////// FUNCTIONS ////////////////////////////
function drawForm( _x,  _y,  _num) {
    push();
    translate(_x, _y);
    let d = mouseX/2;
    line(0,-height,0,height);
    for (let i = 0; i< width; i+=_num) {
      arc(0, 0,  d-i, height, 0, TWO_PI);
    }
  
    pop();
  }

function keyPressed() {
    if(key == '+') sw++;
    if(key == '-') {
      if(sw>0)
      sw--;
    }

}