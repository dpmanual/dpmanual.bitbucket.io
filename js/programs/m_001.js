/**
 * HARMONIC MOTION
 * 
 * Sketch : oscillation_OffSet 
 */

let m; // motion value
let mode = 0;
let gridStep = 40;
let myFont;

function preload() {
  myFont = loadFont('../font/IBMPlexMono-Bold.ttf');
}

function setup() {
  let cnv = createCanvas(500, 500);
  cnv.parent('theCanvas');
  noStroke();
  textFont(myFont);
  textSize(12);
}

function draw() {
  background(0);
  fill(255);
  let f = map(mouseX, 0, width, 0.025, 0.075);
  let amp = map(mouseY, 0, height, 25, 175);
  for (let x = 50; x < width - 50; x += gridStep) {
    switch (mode) {
      case 0:
        m = sin(x + frameCount * 0.020) * 120;
        let d = sin(x + frameCount * 0.10) * 90;
        ellipse(x, height / 2 + m, d, d);
        break;

      case 1:
        m = sin(x + frameCount * f) * amp;
        ellipse(x, height / 2 + m, m / 2, m / 2);
        break;
    }
  }
  text("MODE: " + mode, 15, height - 20);
}

function keyPressed() {
  if (key == '+') {
    mode++;
  }
  if (key == '-') {
    if (mode > 0) {
      mode--;
    }
  }
  if (key == 'l') {
    gridStep += 5;
  }
  if (key == 'm') {
    if (gridStep > 5)
      gridStep -= 5;
  }
}