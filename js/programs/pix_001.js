/**
 * A Grid of Pixels
 * 
 */


/////////////////////////// GLOBALS ////////////////////////////
let colums, rows;
let randSeed = 1;
/////////////////////////// SETUP ////////////////////////////

function setup() {
  let cnv = createCanvas(500, 500);
  cnv.parent('theCanvas');
  background(0);
  noStroke();
}

/////////////////////////// DRAW ////////////////////////////
function draw() {
  background(0);
  randomSeed(randSeed);
  if(mouseY>1){
    let pixelDensity = map(mouseY, 0, height, 1, 73);
    drawPixels(pixelDensity);
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////
function drawPixels( _density) {
  colums = Math.floor(width/_density);
  rows = Math.floor(height/_density);
  for (let i=0; i<=colums; i++) {
    for (let j=0; j<=rows; j++) {
        let randVal = random(12);
        let cutOff = map(mouseX, 0, width, 1, 12);
      if (randVal > cutOff) {
        rect( i*_density, j*_density, _density+1, _density+1);
      }
    }
  }
}

function keyPressed() {
  if (key === 'r') {
    randSeed = random(1000);
  }
  //if (key === 's') saveFrame("savedImage_###.png");
}