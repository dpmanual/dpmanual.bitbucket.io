/**
 * HARMONIC MOTION
 * 
 * Sketch : DPM_003_p5js
 */


let mode = 0;
let gridStep = 12;
let isCombine = false;

function setup() {
  let cnv = createCanvas(500, 500);
  cnv.parent('theCanvas');
  noStroke();
}

function draw() {
    fill(0, 33);
    rect(0, 0, width, height);
    fill(255);
    let h = 0;
    let xOff = map(mouseX, 0, width, 0.015, 0.1);
    for (let x=0; x<width; x+=gridStep) {
      if(isCombine){
       h = sin( (x * xOff) + frameCount * 0.05) * 150; 
      }else {
        h = sin( x + frameCount * 0.05) * 150;
      }
      let y = sin( (x * xOff)  + frameCount * 0.025) * 50; 
      switch(mode) {
      case 0:
        rect(x, height/2 + y, gridStep, h);
        break;
      case 1:
        rect(width/2+y, x, h, gridStep);
        break;
      }
    }
}

function keyPressed() {
  if (key == '+') {
    mode++;
  }
  if (key == '-') {
    if (mode > 0) {
      mode--;
    }
  }
  if (key == 'l') {
    gridStep+=2;
  }
  if (key == 'm') {
    if (gridStep>2)
      gridStep-=2;
  }
  if(key == 'c'){
   isCombine = !isCombine; 
  }
}