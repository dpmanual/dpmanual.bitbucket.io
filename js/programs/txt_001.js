/*
 * Repeating Text
 */

/////////////////////////// GLOBALS ////////////////////////////
let myFont;
let word = "BAUHAUS";
/////////////////////////// SETUP ////////////////////////////

function preload() {
  myFont = loadFont('../font/GRACE_beta1-Black.otf');
}

function setup() {
  let cnv = createCanvas(500, 500);
  cnv.parent('theCanvas');
  background(0);
  noStroke();
  textFont(myFont);
  textSize(80);
}

/////////////////////////// DRAW ////////////////////////////
function draw() {
  background(0);
  if(mouseY>1){
  let interval = map(mouseY, 0, height, 1, 150);
  
  for (let i=75; i<height; i+=interval) {
    let gradient = map(i, 0, height, 0, 255);
    fill(gradient);
    text(word, 50, i);
    }
  }
}

/////////////////////////// FUNCTIONS ////////////////////////////

function keyPressed(){
 if(key == 's'){
  //saveFrame("export_###.png"); 
 }
}