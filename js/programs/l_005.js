/**
* Random lines on a surface
*/

/////////////////////////// GLOBALS ////////////////////////////

let randSeed;
let lineLength = 10;
let sw = 10;
let isRandAngle = false;
/////////////////////////// SETUP ////////////////////////////

function setup() {
    let cnv = createCanvas(500, 500);
    cnv.parent('theCanvas');
  background(0);
  stroke(255);
  strokeCap(SQUARE);
  randSeed = random(5000);
}

/////////////////////////// DRAW ////////////////////////////
function draw() {
  background(0);
  randomSeed(randSeed);
  strokeWeight(sw);
  let angle = 0;
  let numLines = map(mouseX, 0, width, 1, 150);
  let a = map(mouseY, 0, height, 0, 360);
  for (let i=0; i<numLines; i++) {
    if (isRandAngle) {
      angle = random(a);
    } else {
      angle = 0;
    }
    let randLen = random(10, 200);

    let x = random(width);
    let y = random(height);
    push();
    translate(x, y);
    rotate(radians(angle+a));
    line(-randLen-lineLength, 0, randLen+lineLength, 0);
    pop();
  }
}


function keyPressed() {
  if (key == 's') {
    saveFrame("export_DPMOsc_002_###.png");
  }
  if (key == 'r') {
    randSeed = random(5000);
  }
  if (key == 'a') {
    isRandAngle = !isRandAngle;
  }
  if (key == '+') {
    sw+=2;
  }
  if (key == '-') {
    if (sw>0)
      sw-=2;
  }
  if (key == 'l') {
    lineLength+=5;
  }
  if (key == 'm') {
    lineLength-=5;
  }
}
