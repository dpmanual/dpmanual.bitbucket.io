/**
 * A Line
 * 
 */


let thickness = 35;
/////////////////////////// SETUP ////////////////////////////

function setup() {
    let cnv = createCanvas(500, 500);
    cnv.parent('theCanvas');
    background(0);
    smooth();
    strokeCap(SQUARE);
}

/////////////////////////// DRAW ////////////////////////////
function draw() {
    background(0);
    stroke(255);
    let angle = map(mouseX, 0, width, 1, 720);
    let len = map(mouseY, 0, height, 1, 600);
    strokeWeight(thickness);

    push();
    translate(width / 2, height / 2);
    rotate(radians(angle));
    line(-len, -len, len, len);
    pop();
}

/////////////////////////// FUNCTIONS ////////////////////////////
function keyPressed() {
    if (key === '+') {
        thickness += 5;
    }
    if (key === '-') {
        if (thickness > 5) {
            thickness -= 5;
        }
    }
}