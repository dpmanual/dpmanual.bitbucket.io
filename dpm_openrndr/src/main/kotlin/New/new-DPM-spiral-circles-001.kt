package New

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.extensions.Screenshots
import org.openrndr.math.map
import java.lang.Math.cos
import java.lang.Math.sin

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {

        var step = 1.0

        keyboard.character.listen {
            if (it.character == '+') { step++ }
            if (it.character == '-') { step-- }
        }

        extend(Screenshots()) { key = "s" }
        extend {
            drawer.clear(ColorRGBa.BLACK)
            drawer.fill = ColorRGBa.PINK
            drawer.stroke = null

            drawer.translate(width/2.0, height/2.0)

            val num = map(0.0, width.toDouble(), 0.0, 500.0, mouse.position.x).toInt()
            val size = map(0.0, height.toDouble(), 1.0, 50.0, mouse.position.y)

            for (i in 0 until num) {

                val x = cos(i * step) * i
                val y = sin(i * step) * i

                drawer.circle(x, y, size)
            }
        }
    }
}