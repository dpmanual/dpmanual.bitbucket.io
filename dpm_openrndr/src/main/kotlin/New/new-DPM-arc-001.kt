package New

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.extensions.Screenshots
import org.openrndr.math.map
import org.openrndr.shape.contour

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {

        var thickness = 1.0

        keyboard.character.listen {
            if (it.character == '+') { thickness++ }
            if (it.character == '-') { if (thickness > 1.0) { thickness-- } }
        }

        extend(Screenshots()) { key = "s" }
        extend {
            drawer.clear(ColorRGBa.BLACK)
            drawer.stroke = ColorRGBa.PINK
            drawer.fill = null
            drawer.strokeWeight = thickness

            val num = map(0.0, height.toDouble(), 1.0, 75.0, mouse.position.y).coerceIn(1.0, 75.0).toInt()

            val c = contour {
                for (i in 0 .. num) {
                    val linePositionX = width / 2.0
                    val toWidth = map(0.0, num.toDouble(), 0.0, width / 2.0, i.toDouble())
                    val x = map(0.0, width.toDouble(), 0.0, toWidth, mouse.position.x).coerceIn(0.0, toWidth)
                    val y = height / 2.0
                    moveTo(linePositionX, 0.0)
                    arcTo(x, y, 0.0, true, true, linePositionX, height.toDouble())
                    arcTo(x, y, 0.0, true, true, linePositionX, 0.0)
                }
            }

            drawer.contour(c)
        }
    }
}