package New

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.isolated
import org.openrndr.extensions.Screenshots
import org.openrndr.math.map
import java.lang.Math.cos
import java.lang.Math.sin


fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {

        var thickness = 1.0
        var interactive = false

        keyboard.keyUp.listen {
            if (it.name == "=") { thickness++ }
            if (it.name == "-") { thickness-- }
            if (it.name == "i") { interactive = true }
        }

//        extend(ScreenRecorder())
        extend(Screenshots()) { key = "s" }
        extend {
            drawer.clear(ColorRGBa.BLACK)
            drawer.fill = ColorRGBa.BLACK
            drawer.stroke = ColorRGBa.PINK
            drawer.strokeWeight = thickness
            drawer.translate(width/2.0, height/2.0)

            val num = map(0.0, height.toDouble(), 1.0, 150.0, mouse.position.y).toInt()

            for (i in 1 until num) {
                drawer.isolated {

                    val orbitX = 50.0
                    val orbitY = 10.0
                    
                    val y = map(0.0, num.toDouble(), -((height/2.0) - orbitY), (height/2.0) - orbitY, i.toDouble())

                    val delay = map(0.0, num.toDouble(), -1.0, 1.0, i.toDouble())
                    
                    val angle = if(interactive) {
                        map(0.0, width.toDouble(), -1.0, 1.0, mouse.position.x)
                    } else {
                        seconds
                    }

                    val x1 = sin(delay + angle) * orbitX
                    val y1 = y + (cos(delay + angle)) * orbitY
                    val x2 = sin(delay + angle) * (-orbitX)
                    val y2 = y + (cos(delay + angle)) * (-orbitY)

                    drawer.lineSegment(x1, y1, x2, y2)
                }
            }
        }
    }
}