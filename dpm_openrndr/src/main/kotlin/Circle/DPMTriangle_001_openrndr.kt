package Circle

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.LineJoin
import org.openrndr.draw.isolated
import org.openrndr.extensions.Screenshots
import org.openrndr.math.Vector2
import org.openrndr.math.map
import org.openrndr.shape.contour
import kotlin.math.cos
import kotlin.math.sin

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {

        var radius = 50.0
        var sw = 7.0

        fun drawForm(x:Double, y:Double, num:Int, rad:Double, angle:Double) {
            drawer.isolated {

                drawer.translate(x, y)
                drawer.rotate(angle)
                val a = Math.toRadians(360.0/num)

                val c = contour {
                    moveTo(Vector2(-rad/2.0, - rad/2.0))
                    for (i in 0 until num) {
                        val x = cos(i * a) * rad
                        val y = sin(i * a) * rad
                        lineTo(cursor + Vector2(x, y))
                    }
                    lineTo(anchor)
                    close()
                }
//                drawer.lineCap = LineCap.SQUARE
                drawer.lineJoin = LineJoin.BEVEL
                drawer.contour(c)
            }
        }

        keyboard.character.listen {
            if (it.character == '+') { radius+=5.0 }
            if (it.character == '-') { radius-=5.0 }
            if (it.character == 'w') { sw +=2.0 }
            if (it.character == 'x') { if(sw>0){ sw -=2.0 } }
        }

        extend(Screenshots()) { key = "s" }
        extend {
            drawer.clear(ColorRGBa.BLACK)
            drawer.fill = null
            drawer.stroke = ColorRGBa.WHITE
            drawer.strokeWeight = sw

            val angle = map(0.0, height.toDouble(), 0.0, 360.0, mouse.position.y)
            val num = map(0.0, width.toDouble(), 1.0, 60.0, mouse.position.x)

            for(i in 1 until num.toInt()){
                drawForm(width/2.0, height/2.0, 3, radius*i, angle*(i*0.05))
            }
        }
    }
}