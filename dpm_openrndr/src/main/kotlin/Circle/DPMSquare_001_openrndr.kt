package Circle

import org.openrndr.WindowMultisample
import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.isolated
import org.openrndr.extensions.Screenshots
import org.openrndr.math.map

fun main() = application {
    configure {
        width = 500
        height = 500
        multisample = WindowMultisample.SampleCount(8)
    }

    program {

        var num = 7.0
        var isAlternate = false

        fun drawForm(x:Double, y:Double, angle:Double, num:Double) {

            drawer.isolated {
                drawer.translate(x, y)

                val shpSize = num * 15.0
                var a = 0.0

                for (i in 0 until width) {
                    if(isAlternate){
                        if(i%2==0){
                            drawer.rotate(a*i)
                        } else {
                            drawer.rotate(-a*i)
                        }
                    } else {
                        drawer.rotate(a*i)
                    }

                    val size = 10+shpSize*i

                    drawer.rectangle(-(size/2), -(size/2), size, size)
                    a += angle
                }
            }
        }

        keyboard.character.listen {
            if (it.character == '+') { num++ }
            if (it.character == '-') { num-- }
            if (it.character == 'a') { isAlternate =!isAlternate }
        }

        extend(Screenshots()) { key = "s" }
        extend {
            drawer.clear(ColorRGBa.BLACK)
            drawer.fill = null
            drawer.stroke = ColorRGBa.WHITE

            val sw = map(0.0, height.toDouble(), 1.0, 50.0, mouse.position.y)
            drawer.strokeWeight = sw
            val angle = map( 0.0, width.toDouble(), 0.0, 0.35, mouse.position.x)

            drawForm(width/2.0, height/2.0, angle, num)
        }
    }
}