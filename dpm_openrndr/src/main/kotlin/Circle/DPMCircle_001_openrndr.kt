package Circle

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.isolated
import org.openrndr.extensions.Screenshots
import org.openrndr.math.map

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {

        var isProgressive = false
        var dia = 0.0

        fun drawForm(x:Double, y:Double, num:Double) {
            drawer.isolated {
                translate(x, y)
                if (!isProgressive) {
                    dia = num * 15.0
                }

                for (i in 0 until 40) {
                    if (isProgressive) {
                        dia = num * i
                    }
                    drawer.circle(0.0,0.0, dia*i)
                }
            }
        }

        keyboard.character.listen {
            if (it.character == 'p') { isProgressive =!isProgressive }
        }

        extend(Screenshots()) { key = "s" }
        extend {
            drawer.clear(ColorRGBa.BLACK)
            drawer.fill = null
            drawer.stroke = ColorRGBa.WHITE

            val sw = map(0.0, height.toDouble(), 1.0, 25.0, mouse.position.y)
            drawer.strokeWeight = sw
            val num = map( 0.0, width.toDouble(), 10.0, 2.5, mouse.position.x)

            drawForm(width/2.0, height/2.0, num)
        }
    }
}