package Motion

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.loadFont
import org.openrndr.extensions.Screenshots
import org.openrndr.extra.noclear.NoClear
import org.openrndr.math.map
import kotlin.math.sin


fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {
        var m = 5.0
        var mode = 0
        var gridStep = 40
        var isBlur = false
        val font = loadFont("dpm_openrndr/data/fonts/IBMPlexMono-Regular.ttf", 12.0)

        keyboard.character.listen {
            when (it.character) {
                'b' -> isBlur = !isBlur
                '+' -> if (mode < 6) mode++
                '-' -> if (mode > 0) mode--
                'l' -> gridStep += 5
                'm' -> if (gridStep > 5) gridStep -= 5
            }
        }

        extend(NoClear())
        extend(Screenshots()) { key = "s" }
        extend {
            drawer.stroke = null
            if (isBlur) {
                drawer.fill = ColorRGBa.BLACK.opacify(0.13)
                drawer.rectangle(drawer.bounds)
            } else {
                drawer.clear(ColorRGBa.BLACK)
            }
            drawer.fontMap = font
            drawer.fill = ColorRGBa.WHITE

            val f = mouse.position.x.map(0.0, width.toDouble(), 0.025, 0.075)
            val amp = mouse.position.y.map(0.0, height.toDouble(), 12.5, 37.5)
            for (y in 25 until height - 25 step gridStep) {
                for (x in 25 until width - 25 step gridStep) {
                    when (mode) {
                        0 -> {
                            m = sin(x + y + frameCount * f) * amp
                            drawer.circle(x.toDouble(), y.toDouble(), m)
                        }
                        1 -> {
                            m = sin(x * y + frameCount * f) * amp
                            drawer.circle(x.toDouble(), y.toDouble(), m)
                        }
                        2 -> {
                            m = sin(x * f + frameCount * f) * amp
                            drawer.circle(x.toDouble(), y.toDouble(), m)
                        }
                        3 -> {
                            m = sin(x * y + frameCount * f) * amp
                            drawer.circle(x + m, y.toDouble(), m)
                        }
                        4 -> {
                            m = sin(x * f + frameCount * f) * amp
                            drawer.circle(x.toDouble(), y + m, m)
                        }
                        5 -> {
                            m = sin(y * f + frameCount * f) * amp
                            drawer.circle(x + m, y.toDouble(), m)
                        }
                        6 -> {
                            m = sin(y * f + frameCount * f) * amp
                            val m2 = sin(x * f + frameCount * f) * amp * 2
                            drawer.circle(x + m + m2, y.toDouble(), m)
                        }
                    }
                }
            }
            drawer.text("Motion Mode: $mode", 15.0, height - 20.0)
        }
    }
}
