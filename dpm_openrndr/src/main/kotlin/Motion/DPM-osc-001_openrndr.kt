package Motion

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.loadFont
import org.openrndr.extensions.Screenshots
import org.openrndr.math.map
import kotlin.math.sin


fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {
        var m = 5.0
        var mode = 0
        var gridStep = 40
        val font = loadFont("dpm_openrndr/data/fonts/IBMPlexMono-Regular.ttf", 12.0)

        keyboard.character.listen {
            when (it.character) {
                '+' -> mode++
                '-' -> if (mode > 0) mode--
                'l' -> gridStep += 5
                'm' -> if (gridStep > 5) gridStep -= 5
            }
        }

        extend(Screenshots()) { key = "s" }
        extend {
            drawer.clear(ColorRGBa.BLACK)
            drawer.stroke = null
            drawer.fontMap = font
            drawer.fill = ColorRGBa.WHITE

            val f = mouse.position.x.map(0.0, width.toDouble(), 0.025, 0.075)
            val amp = mouse.position.y.map(0.0, height.toDouble(), 25.0, 175.0)
            for (x in 50 until width - 50 step gridStep) {
                when (mode) {
                    0 -> {
                        m = sin(x + frameCount * 0.020) * 120
                        val d = sin(x + frameCount * 0.10) * 45
                        drawer.circle(x.toDouble(), height / 2 + m, d)
                    }
                    1 -> {
                        m = sin(x + frameCount * f) * amp
                        drawer.circle(x.toDouble(), height / 2 + m, m / 8)
                    }
                }
            }
            drawer.text("MODE: $mode", 15.0, height - 20.0)
        }
    }
}
