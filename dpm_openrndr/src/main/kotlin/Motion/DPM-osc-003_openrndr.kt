package Motion

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.extensions.Screenshots
import org.openrndr.extra.noclear.NoClear
import org.openrndr.math.map
import kotlin.math.sin

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {
        var mode = 0
        var gridStep = 40.0

        keyboard.character.listen {
            when (it.character) {
                '+' -> mode++
                '-' -> if (mode > 0) mode--
                'l' -> gridStep += 2
                'm' -> if (gridStep > 2) gridStep -= 2
            }
        }

        extend(NoClear())
        extend(Screenshots()) { key = "s" }
        extend {
            drawer.fill = ColorRGBa.BLACK.opacify(0.13)
            drawer.rectangle(drawer.bounds)
            drawer.fill = ColorRGBa.WHITE
            val v = mouse.position.x.map(0.0, width.toDouble(), 0.015, 0.1)
            for (x in 0 until width step gridStep.toInt()) {
                val h = sin(x + frameCount * 0.05) * 150
                val y = sin(x * v + frameCount * 0.025) * 50
                when (mode) {
                    0 -> drawer.rectangle(x.toDouble(), height / 2 + y, gridStep, h)
                    1 -> drawer.rectangle(width / 2 + y, x.toDouble(), h, gridStep)
                }
            }
        }
    }
}
