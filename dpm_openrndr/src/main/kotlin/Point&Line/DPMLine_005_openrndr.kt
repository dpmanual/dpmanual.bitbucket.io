package point_and_line

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.LineCap
import org.openrndr.draw.isolated
import org.openrndr.extensions.Screenshots
import org.openrndr.extra.noise.Random
import org.openrndr.extra.noise.uniform
import org.openrndr.math.map

// activate "orx-noise" in build.gradle.kts

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {
        var randSeed = "1"
        var lineLength = 10
        var sw = 30
        var isRandAngle = false

        extend(Screenshots()) { key = "s" }
        extend {
            Random.seed = randSeed
            drawer.clear(ColorRGBa.BLACK)
            drawer.stroke = ColorRGBa.WHITE
            drawer.strokeWeight = sw.toDouble()
            drawer.lineCap = LineCap.SQUARE
            val numLines = mouse.position.x.map(
                0.0, width.toDouble(), 1.0, 150.0
            )
            val a = mouse.position.y.map(
                0.0, height.toDouble(), 0.0, 360.0
            )
            repeat(numLines.toInt()) {
                val angle = if (isRandAngle && a > 0) Random.double0(a) else 0.0
                val randLen = Random.double(10.0, 200.0)

                drawer.isolated {
                    translate(drawer.bounds.uniform())
                    rotate(angle + a)
                    lineSegment(
                        -randLen - lineLength, 0.0,
                        randLen + lineLength, 0.0
                    )
                }
            }
        }

        keyboard.character.listen {
            when (it.character) {
                'r' -> randSeed = Random.int0(5000).toString()
                'a' -> isRandAngle = !isRandAngle
                '+' -> sw += 5
                '-' -> if (sw > 0) sw -= 5
                'l' -> lineLength += 5
                'm' -> lineLength -= 5
            }
        }
    }
}