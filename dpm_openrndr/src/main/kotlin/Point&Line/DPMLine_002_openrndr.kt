package point_and_line

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.extensions.Screenshots
import org.openrndr.extra.noise.valueLinear
import org.openrndr.math.map

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {

        var randSeed = 1

        keyboard.character.listen {
            if (it.character == 'r') { randSeed = (Math.random() * 1000).toInt() }
        }

        extend(Screenshots()) { key = "s" }
        extend {
            drawer.clear(ColorRGBa.BLACK)
            drawer.stroke = ColorRGBa.WHITE

            val sw = map(0.0, width.toDouble(), 1.0, 100.0, mouse.position.x).toInt()
            drawer.strokeWeight = 1.0 + sw

            if (sw > 1.0) {
                for (x in 0 until width step sw) {
                    val rndHeight = valueLinear(randSeed, x/sw.toDouble(), 0.0) * height + height
                    drawer.lineSegment(x.toDouble(), 0.0, x.toDouble(), height - rndHeight)
                }
            }
        }
    }
}