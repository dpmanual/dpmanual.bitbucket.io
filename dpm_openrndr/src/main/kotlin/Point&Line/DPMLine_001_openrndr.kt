package point_and_line

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.isolated
import org.openrndr.extensions.Screenshots
import org.openrndr.math.map

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {

        var thickness = 35.0

        keyboard.character.listen {
            if (it.character == '+') { thickness += 5.0 }
            if (it.character == '-') { if (thickness > 5.0) { thickness -= 5.0 } }
        }

        extend(Screenshots()) { key = "s" }
        extend {
            drawer.clear(ColorRGBa.BLACK)
            drawer.stroke = ColorRGBa.WHITE

            val angle = map(0.0, width.toDouble(), 1.0, 720.0, mouse.position.x)
            val len = map(0.0, height.toDouble(), 1.0,600.0, mouse.position.y)

            drawer.strokeWeight = thickness
            drawer.isolated {
                translate(width/2.0, height/2.0)
                rotate(angle)
                drawer.lineSegment(-len, -len, len, len)
            }

        }
    }
}