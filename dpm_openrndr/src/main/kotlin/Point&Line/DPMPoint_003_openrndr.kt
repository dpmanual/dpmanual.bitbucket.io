package point_and_line

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.extensions.Screenshots
import org.openrndr.extra.noise.valueLinear
import org.openrndr.math.map

// activate "orx-noise" in build.gradle.kts

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {

        var randSeed = 1

        keyboard.character.listen {
            if (it.character == 'r') { randSeed = (Math.random() * 1000).toInt() }
        }

        extend(Screenshots()) { key = "s" }
        extend {
            drawer.clear(ColorRGBa.BLACK)
            drawer.fill = ColorRGBa.WHITE
            drawer.stroke = null

            val numPoints = map(0.0, width.toDouble(), 1.0, 500.0, mouse.position.x).coerceAtLeast(1.0).coerceAtMost(500.0).toInt()
            val cutOffVal = map(0.0, height.toDouble(), 1.0, 62.5, mouse.position.y).coerceAtLeast(1.0).coerceAtMost(62.5)

            for (i in 0 until numPoints) {

                val randValX = valueLinear(randSeed, i.toDouble(), 0.0) * 1.0 + 1.0
                val randValY = valueLinear(randSeed, 0.0, i.toDouble()) * 1.0 + 1.0
                val randValS = valueLinear(randSeed, i.toDouble(), i.toDouble()) * 1.0 + 1.0
                val x = randValX * width
                val y = randValY * height
                val pntSize = randValS * cutOffVal

                drawer.circle(x, y, pntSize)
            }
        }
    }
}