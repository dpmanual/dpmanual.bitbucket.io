package point_and_line

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.extensions.Screenshots
import org.openrndr.math.Vector2
import org.openrndr.math.map
import org.openrndr.shape.Segment

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {
        extend(Screenshots()) { key = "s" }
        extend {
            drawer.clear(ColorRGBa.BLACK)

            val num = mouse.position.x.map(
                0.0, width.toDouble(), 1.0, 75.0
            ).toInt()
            val thickness = mouse.position.y.map(
                0.0, height.toDouble(), 1.0, 5.0
            )
            drawer.fill = null
            drawer.stroke = ColorRGBa.WHITE
            drawer.strokeWeight = thickness
            repeat(num) {
                val linePositionX =
                    it.toDouble().map(0.0, num.toDouble(), 10.0, width - 10.0)
                val bezierSegment = Segment(
                    Vector2(linePositionX, 0.0),
                    mouse.position,
                    mouse.position,
                    Vector2(linePositionX, height.toDouble())
                )
                drawer.segment(bezierSegment)
            }
        }
    }
}