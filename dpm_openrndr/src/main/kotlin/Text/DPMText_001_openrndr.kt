package Text

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.draw.loadFont
import org.openrndr.extensions.Screenshots
import org.openrndr.math.map

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {

        val font = loadFont("dpm_openrndr/data/fonts/FiraCode-Bold.otf", 110.0)
        val word = "BAUHAUS"

        extend(Screenshots()) { key = "s" }
        extend {
            drawer.clear(ColorRGBa.BLACK)
            drawer.fill = ColorRGBa.WHITE
            drawer.stroke = null

            val mouseY  = if (mouse.position.y > 0) { mouse.position.y } else { 0.0 }
            val maxIteration = map(0.0, 500.0, 75.0, 500.0, mouse.position.x).toInt()
            val interval = map(0.0, 500.0, 1.0, 100.0, mouseY).toInt()

            drawer.fontMap = font

            (75 .. maxIteration step interval).forEach { i ->
                val gradient = map(0.0, height.toDouble(), 0.0, 1.0, i.toDouble())
                drawer.fill = ColorRGBa(gradient, gradient, gradient, 1.0)

                drawer.text(word, 50.0, i.toDouble())
            }
        }
    }
}