package Texture

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.color.rgb
import org.openrndr.draw.LineCap
import org.openrndr.draw.isolated
import org.openrndr.extensions.Screenshots
import org.openrndr.extra.noise.Random

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {
        var interval = 10
        var seed = Random.int0(1000)
        var sw = 2.0
        var gridStep = 100

        fun line(x0: Int, y0: Int, x1: Int, y1: Int) = drawer.lineSegment(
            x0.toDouble(), y0.toDouble(), x1.toDouble(), y1.toDouble()
        )
        fun form(x: Int, y: Int, size: Int, type: Int) {
            drawer.stroke = rgb(Random.double0())
            drawer.isolated {
                translate(x.toDouble(), y.toDouble())
                for (i in 0 until size step interval) {
                    val j = size - 1
                    when (type) {
                        0 -> line(i, 0, size, j)
                        1 -> line(10, i, j + 10, size)
                        2 -> line(i, size, size, i)
                    }
                }
            }
        }

        extend(Screenshots()) { key = "s" }
        extend {
            Random.seed = seed.toString()
            drawer.clear(ColorRGBa.BLACK)
            drawer.strokeWeight = sw
            drawer.lineCap = LineCap.SQUARE

            for (x in 0 until width - 30 step gridStep) {
                for (y in 0 until height - 30 step gridStep) {
                    form(x, y, gridStep, 0)
                    form(x - 10, y, gridStep, 1)
                    form(x, y, gridStep, 2)
                }
            }
        }

        keyboard.character.listen {
            when (it.character) {
                'r' -> seed = Random.int0(1000)
                '+' -> interval++
                '-' -> interval--
                'w' -> sw++
                'x' -> sw--
                'l' -> gridStep += 25
                'm' -> if(gridStep > 50) gridStep -= 25
            }
        }
    }
}