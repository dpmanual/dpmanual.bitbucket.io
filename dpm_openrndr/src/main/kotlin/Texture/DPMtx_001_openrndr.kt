package Texture

import org.openrndr.application
import org.openrndr.color.ColorRGBa
import org.openrndr.extensions.Screenshots
import org.openrndr.math.map
import org.openrndr.shape.Circle

fun main() = application {
    configure {
        width = 500
        height = 500
    }

    program {

        var interval = 10

        keyboard.character.listen {
            when (it.character) {
                '+' -> interval++
                '-' -> interval = (interval - 1).coerceAtLeast(0)
            }
        }

        fun drawGrid(d: Double) {
            val circles = mutableListOf<Circle>()
            for (y in -height/2*2 until height-50 step interval) {
                for (x in -width/2*2 until width-50 step interval) {
                    circles.add(Circle(x.toDouble(), y.toDouble(), d))
                }
            }
            drawer.circles(circles)
        }

        extend(Screenshots()) { key = "s" }
        extend {
            drawer.clear(ColorRGBa.BLACK)
            drawer.fill = ColorRGBa.WHITE
            drawer.stroke = null
            drawer.translate(drawer.bounds.center)

            val dia2 = mouse.position.y.map(0.0, height.toDouble(), 1.0, 10.0)
            drawGrid(dia2)
            drawer.rotate(mouse.position.x * 0.05)
            drawGrid(dia2)
        }
    }
}